#!/usr/bin/bash


zip -r minimesh-v${1}.zip \
    .vscode \
    build-dbg/.gitignore \
    build-opt/.gitignore \
    cmake \
    debugger \
    mesh \
    third-party \
    .gitattributes \
    .gitignore \
    cmake-examples.txt \
    minimesh \
    readme.txt
