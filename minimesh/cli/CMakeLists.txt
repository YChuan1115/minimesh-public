cmake_minimum_required(VERSION 3.5.0)
project(minimeshcli)

set(TARGET_NAME ${PROJECT_NAME})


##
## SOURCE FILES
##
set(SOURCE_FILES
  main.cpp
)

 
add_executable(${TARGET_NAME}  ${SOURCE_FILES})
minimesh_setup_exe_location(${TARGET_NAME})
target_link_libraries(${TARGET_NAME}
  minimeshcore
)

##
## Create a folder structure inside the visual studio project
##
minimesh_folders_lib(${TARGET_NAME}  ${SOURCE_FILES})
